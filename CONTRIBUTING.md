package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class MontoEscrito {
    private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un valor:");
        int v=sc.nextInt();
        crearParseNumeros();
        String contenido=parseoNumeros.get(v);
        System.out.println( "Su valor en letras es:"+contenido);
    }

    public static void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
        parseoNumeros.put(9156, "nueve mil ciento cincuenta y seis");
        parseoNumeros.put(3290641, "tres millones doscientos noventa mil seiscientos cuarenta y uno");
    }

}